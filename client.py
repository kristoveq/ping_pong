import socket
import json


class Client:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 60000
        self.buffer = 1024
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.id = None
        self.is_done = False

    def connect(self):
        self.client_socket.connect((self.host, self.port))

    def take_data(self):
        response = json.loads(self.client_socket.recv(self.buffer).decode('utf-8'))
        if response:
            return response
        print('Error with msg')

    def send_data(self, request):
        json_request = json.dumps(request)
        self.client_socket.send(json_request.encode('utf-8'))


clients_list = []

for x in range(100):
    client = Client()
    client.connect()
    clients_list.append(client)

while True:
    for client in clients_list:
        data = client.take_data()
        print(data)
        if data.split(',')[0] == 'ok':
            client.id = data.split(',')[1]

        elif data == 'PING':
            client.send_data(f'PONG, {client.id}')
            print(f'Client {client.id} send PONG')
            client.is_done = True

    done = all(client.is_done for client in clients_list)
    if done:
        for single_client in clients_list:
            single_client.client_socket.close()
        break
