import socket
import json
from data_tools import Logger
import threading


class MultiThreads(threading.Thread,):
    def __init__(self, socket, address, thread_id):
        super().__init__()
        self.thread_id = thread_id
        self.socket = socket
        self.address = address
        self.buffer = 1024
        self.feedback = False

    def run(self):
        print(f'Thread no. {self.thread_id} in action.')

    def send_it(self, data):
        if data:
            json_data = json.dumps(data)
            self.socket.send(json_data.encode('utf-8'))
        else:
            print('Trying to send empty data.')

    def take_it(self):
        response = json.loads(self.socket.recv(self.buffer).decode('utf-8'))
        return response


class Server:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 60000
        self.buffer = 1024
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger = Logger()
        self.clients_list = []
        self.connection_capacity = 100
        self.lock = threading.Lock()

    def connect(self):
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(self.connection_capacity)
        self.logger.log_info('Server connected\n')

    def create_and_run_threads(self):
        for x in range(self.connection_capacity):
            client_socket, client_address = self.server_socket.accept()
            thread = MultiThreads(client_socket, client_address, x + 1)
            self.clients_list.append(thread)
            thread.start()

    def send_ok_msg(self):
        for thread in self.clients_list:
            with self.lock:
                thread.send_it(f'ok, {thread.thread_id}')

    def send_ping_msg(self):
        if len(self.clients_list) == self.connection_capacity:
            for thread in self.clients_list:
                with self.lock:
                    thread.send_it('PING')
                    self.logger.log_info(f'Thread no. {thread.thread_id} send PING msg')

    def get_pong_msg(self):
        for thread in self.clients_list:
            with self.lock:
                data = thread.take_it()
                if data.split(',')[0] == 'PONG':
                    thread.feedback = True
                    self.logger.log_info(f'Get PONG msg from Client no. {thread.thread_id}. ')
                    thread.socket.close()


server = Server()
server.connect()
server.create_and_run_threads()

while True:
    server.send_ok_msg()
    server.send_ping_msg()
    server.get_pong_msg()

    all_feedback = all(thread.feedback for thread in server.clients_list)
    if all_feedback:
        server.logger.log_info(f'Get all ({server.connection_capacity}) responses.\nEnd of action')
        break
