import json
import logging

class JsonType:
    def serialize(self, data):
        result = json.dumps(data)
        return result

    def deserialize(self, data):
        result = json.loads(data)
        return result


class Logger:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

    def log_info(self, message):
        self.logger.info(message)


